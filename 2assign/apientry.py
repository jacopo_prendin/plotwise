
# -*- coding: utf-8 -*-

class APIEntry:
    '''
    This class contains all informations from an API call, including  its input
    parameters and its results
    '''
    def __init__(self, origin_point, destination_point,
                 origin_name, destination_name,
                 api_inputs, row_elements):
        self.origin_point = origin_point
        self.destination_point = destination_point
        self.origin_name = origin_name
        self.destination_name = destination_name

        # api inputs
        self.avoid = api_inputs['avoid']
        self.units = api_inputs['units']
        self.travel_mode = api_inputs['travel_mode']
        self.transit_mode = api_inputs['transit_mode']
        self.departure_time = api_inputs['departure_time']
        self.arrival_time = api_inputs['arrival_time']
        self.traffic_model = api_inputs['traffic_model']

        # output
        self.distance_text = row_elements['distance']['text']
        self.distance_value = row_elements['distance']['value']
        self.duration_text = row_elements['duration']['text']
        self.duration_value = row_elements['duration']['value']
        self.duration_in_traffic_text = row_elements['duration_in_traffic']['text']
        self.duration_in_traffic_value = row_elements['duration_in_traffic']['value']
        
    def __repr__(self):
        return 'from: {0}\nto: {1}\n    > distance = {2} time = {3} ({4} secs)'.format(
            self.origin_name,
            self.destination_name,
            self.distance_text,
            self.duration_text,
            self.duration_value)
