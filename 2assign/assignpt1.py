
# -*- coding: utf-8 -*-

import datetime as dt
import json
import urllib2

import config
from apientry import APIEntry
from pathcoords import PathCoords

class APICaller:
    '''
    This class offers an interface to the Google Matrix API call. Its method
    "execute_call" returns the result of the API as a list of objects APIEntry,
    easy to analize for testing
    '''
    def __init__(self, api_address, api_key):
        self.address = api_address
        self.key = api_key

        self.default_departure_time = self._get_time_next(minutes = 10)
        self.default_arrival_time = self._get_time_next(minutes = 30)

    def _get_time_next(self,minutes = 0):
        '''
        This private method returns the UNIX timestamp (seconds from January
        1st 1970) of now PLUS a certain amount of minutes.
        '''
        delta_t = dt.timedelta(seconds = minutes * 60.0)
        t = dt.datetime.now() + delta_t
        return t.strftime("%s")    
 
    def execute_call(self, origins, destinations, **kwargs):
        '''
        Executes the API call using the coordinates passed as input and returns
        a list of APIEntries with results. The API other parameters, defined at

        https://developers.google.com/maps/documentation/distance-matrix/intro

        can be passed as normal parameters, thanks to the **kwargs input

        @param origins: List of PathCoords points where we want just the
            starting position
        @param destinations" List of PathCoords points
        @param **kwargs: other parameters defined by the Google API
        @return a list of APIEntry objects
        '''

        
        parameters = {}
        parameters['language'] = kwargs.get('language','en_EN')
        parameters['avoid'] = kwargs.get('avoid','indoor')
        parameters['units'] = kwargs.get('units','metric')
        parameters['travel_mode']=kwargs.get('travel_mode','driving')
        parameters['transit_mode'] = kwargs.get('transit_mode','')
        parameters['traffic_model'] = kwargs.get('traffic_model','best_guess')

        parameters['departure_time'] = kwargs.get(
            'departure_time',
            self.default_departure_time)
        parameters['arrival_time'] = kwargs.get(
            'arrival_time',
            self.default_arrival_time)


        # create the API call using the base address, the API key and all the
        # parameters we used
        api_call = self.address + '&key=' + self.key 
        for key in parameters:
            api_call += '&{0}={1}'.format(key,parameters[key])

        # appends origin and destination
        origins_lst = [o.start_as_str() for o in origins]
        destinations_lst = [d.end_as_str() for d in destinations]
        origins_str = ('|'.join(origins_lst))[:-1]
        destinations_str = ('|'.join(destinations_lst))[:-1]

        api_call += '&origins={0}&destinations={1}'.format(
            origins_str,
            destinations_str)

        # execute the call
        print(api_call)
        contents = urllib2.urlopen(api_call).read()

        reply = json.loads(contents)

        # return an array of entries
        entries = []
        for o in range(0, len(origins)):
            dest_infos = reply['rows'][o]['elements']
            for d in range(0,len(destinations)):
                new_entry = APIEntry(origins[o],destinations[d],
                                     reply['origin_addresses'][o],
                                     reply['destination_addresses'][d],
                                     parameters,
                                     dest_infos[d])
                entries.append(new_entry)

        return entries


# ==============================================================================
# 
#                                  MAIN
# 
# ==============================================================================

if __name__ == '__main__':
    reference = None
    for i in range(0,12):
        # verify distance doesn't change
        tester = APICaller(config.API_CALL,config.API_KEY)
        matrix = tester.execute_call(config.points[0:2],
                                     config.points[2:])

        if i == 0:
            reference = matrix
        else:
            assert len(reference) == len(matrix)
            for entry_num in range(0,len(reference)):
                ref_entry = reference[entry_num]
                specimen = matrix[entry_num]

                # distance can't change
                assert ref_entry.distance_value == specimen.distance_value,"Wrong distance value"


