
# -*- coding: utf-8 -*-

from pathcoords import PathCoords

API_CALL='https://maps.googleapis.com/maps/api/distancematrix/json?'
API_KEY='AIzaSyCcbTVVgrHnQY8G-zgBmw1rmtsNxgO3Us4'


points = [
    PathCoords(51.72756055,5.547473487, 51.673471,5.604358),
    PathCoords(51.6134128,5.453728131, 51.673471,5.604358),
    PathCoords(50.6772072,5.5959908, 50.814113,5.166075),
    PathCoords(50.9442881,5.4674197, 50.814113,5.166075),
    PathCoords(52.3802093,4.872755698, 51.953257,4.55655),
    PathCoords(52.34397807,4.825849925, 51.953257,4.55655)
]
