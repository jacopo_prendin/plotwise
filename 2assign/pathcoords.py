
# -*- coding: utf-8 -*-

class PathCoords:
    '''
    Helper class to manage points: it contains the starting and ending 
    coordinates as latitude and longitute. Its methods returns these two
    points as strings for the API callx
    '''
    def __init__(self, start_latitude, start_longitude,
                 end_latitude, end_longitude):
        self.start_longitude = start_longitude
        self.start_latitude = start_latitude
        self.end_longitude = end_longitude
        self.end_latitude = end_latitude

    def start_as_str(self):
        return '{0},{1}'.format(self.start_latitude,self.start_longitude)

    def end_as_str(self):
        return '{0},{1}'.format(self.end_latitude,self.end_longitude)
