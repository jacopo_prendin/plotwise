
# -*- coding: utf-8 -*-

import datetime as dt
import time
import csv

import config
from assignpt1 import APICaller

def entry_to_tuple(entry):
    '''
    Transform an APIEntry object to a tuple, ready to feed the csv writer
    '''
    return (entry.origin_point.start_as_str(),
            entry.destination_point.end_as_str(),
            entry.origin_name,
            entry.destination_name,
            # api inputs
            entry.avoid,
            entry.units,
            entry.travel_mode,
            entry.transit_mode,
            entry.departure_time,
            entry.arrival_time,
            entry.traffic_model,
            # output
            entry.distance_text,
            entry.distance_value,
            entry.duration_text,
            entry.duration_value,
            entry.duration_in_traffic_text,
            entry.duration_in_traffic_value)

# ==============================================================================
# 
#                                  MAIN
# 
# ==============================================================================

while 1:
    start_timer = dt.datetime.now()
    for i in range(0,len(config.points)):
        tester = APICaller(config.API_CALL,config.API_KEY)
        matrix = tester.execute_call([config.points[i]],
                                     [config.points[i]])
        with open('./file.csv', "ab") as csv_file:
            writer = csv.writer(csv_file,
                                delimiter=';',
                                quotechar='|')
            for entry in matrix:
                writer.writerow( entry_to_tuple(entry) )


    # wait exactly 10 minutes (60 seconds * 10)
    elapsed = dt.datetime.now() - start_timer

    diff = 600 - elapsed.seconds
    if diff > 0:
        time.sleep(diff)
