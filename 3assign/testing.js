
assert = chai.assert;
expect = chai.expect;

/**
 * Helper function to look on a planned_route array, looking for the element 
 * with ID we're looking for. Returns null if object is not found
 */
function get_planned_of_ID(plnobj, id) {
    plnobj.planned_route.deliveries.forEach(function(dlv){
        if (deliv.id == dlv.id) {
            return dlv;
        }
    });

    return null;
}
/**
 * All deliveries from “deliveries_for_planning.json” that are in current_state
 * “planned” are present in “planned_route.json”
*/
function test_a(planned_obj, deliveries_obj) {
    
    deliveries_obj.forEach(function(deliv) {
        // if we have a delivery setted as "planned"
        if (deliv.current_state == "planned") {
            // check if element has been found on planned
            expect( get_planned_of_ID(planned_obj,deliv.id) != null).to.equal(true);
        }
    });
}

/**
 * The sum of weights of the deliveries is less than the carrying_capacity of 
 * the vehicle
 */
function test_b(planned_obj) {
    var total_weight = 0
    planned_obj.deliveries.forEach(function(delivery) {
        total_weight += 
    });
}

/**
 * All eta-s of deliveries in planning (estimated time of arrivals) with “type”
 * delivery are within the route_min_time and route_max_time
 */
function test_c(planned_obj, deliveries_obj) {
    var route_max_time = planned_obj.planned_route.route_max_time;
    var route_min_time = planned_obj.planned_route.route_min_time;
    
    planned_obj.planned_route.deliveries.forEach(function(deliv){
        if (deliv.type == "delivery") {
            assert.equal(
                (deliv.eta > route_min_time &&
                 deliv.eta < route_max_time),
                true,
                "ETA withing route_min_time and route_max_time");
        }
    });
}
/**
 * All eta-s of deliveries in planning are within their delivery_min_time and
 * delivery_max_time
 */
function test_d(delivery_obj) {
    delivery_obj.forEach(function(deliv){

        /*
         * if a delivery is labeled as "planned", get its informations from
         * planned obj and verify that its eta is in [min_time:max_time]
         */
        if (deliv.current_state == "planned") {
            var planned = get_planned_of_ID(planned_obj, deliv.id);
            assert.to(
                (planned.eta >= deliv.min_time &&
                 planned.eta <= deliv.max_time),
                true
            );
        }
    });
}

/**
 * The travel_time_to next (in seconds) is less than or equal to the time 
 * difference between any 2 consecutive deliveries in “planned_route.json”
 */
function test_e(){

}
