
* Assignment 1 - Testing Plan  

** Features Tests
|----+-----------------+----------------+--------+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|    | Orders in queue | Need Groceries | Refuel | Deposit | Expect                                                                                                                                                           |
|----+-----------------+----------------+--------+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  1 |              20 | no             | no     | no      | 20 stops                                                                                                                                                         |
|  2 |              20 | no             | no     | yes     | 21 of stops, the first is the nearest back                                                                                                                       |
|  3 |              20 | no             | yes    | no      | 21 stops, the first to the nearest gas station                                                                                                                   |
|  4 |              20 | no             | yes    | yes     | 22 stops, the first to the back, the second to gas station                                                                                                       |
|  5 |              20 | yes            | no     | no      | 21 stops, first to the groceries                                                                                                                                 |
|  6 |              20 | yes            | no     | yes     | 22 stops, first to deposit, second to groceries                                                                                                                  |
|  7 |              20 | yes            | yes    | no      | 22 stops, first to refuel, second to groceries                                                                                                                   |
|  8 |              20 | yes            | yes    | yes     | 23 stops, first to deposit, second to refuel, third to the groceries                                                                                             |
| 9  |             >20 | no             | no     | no      | 1 stop, to the nearest parking lot to cook                                                                                                                       |
| 10 |            >=20 | X              | X      | X       | Deliveries with an ETA greather than $TOLLERANCE_MINUTES should be coocked in a second moment. We expect, in this case, a path with an intermediate stop to cook |


** Function Tests
1. Planning should work when we have at least 20 orders
2. Deliveries outside range-of-availability should be ignored
3. If we have a really sparse distribution of deliveries (tot chilometers >= vehicle autonomy), we need an intermediate stop to a gas station
4. Closed gas stations should be ignored
5. Closed banks should be ignored
6. Closed groceries should be ignored
7. In case of really sparse deliveries, the option to take an highway paying a tool should be available
8. For each delivery, ETAs are in [min_time:max_time]
9. ETAs cannot be before now 
10. Distances cannot be negative numbers

** Performace Tests
1. Plan for 80, 160, 1200 deliveries
2. Verify response time when we have 60 deliveries, need to refuel and to deposit
3. Monitor the software for one day creating jobs of 80 deliveries
4. Create a series on different requests, alterning high and low amount of deliveries (e.g. repeating the sequence 20, 40, 80, 240, 400, 1200, 400, 20)
5. Check time to get the plans for 20, 80, 160 and 1200 deliveries
